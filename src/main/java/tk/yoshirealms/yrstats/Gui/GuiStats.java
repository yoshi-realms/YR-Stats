package tk.yoshirealms.yrstats.Gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class GuiStats implements Listener {

    public void open(Player player) {


        Inventory inv = GuiBase.create("Statistics", 5);

        ItemStack clan = new ItemStack(Material.BANNER);

        ItemStack perks = new ItemStack(Material.TRIPWIRE_HOOK);

        ItemStack profile = new ItemStack(Material.SKULL_ITEM);

        ItemStack KD = new ItemStack(Material.IRON_SWORD);

        ItemStack Achievments = new ItemStack(Material.NETHER_STAR);

        ItemMeta clanMeta = clan.getItemMeta();
        ItemMeta perksMeta = perks.getItemMeta();
        SkullMeta profileMeta = (SkullMeta) profile.getItemMeta();
        ItemMeta KDMeta = KD.getItemMeta();
        ItemMeta AchievmentsMeta = profile.getItemMeta();

        clanMeta.setDisplayName("Clan");
        perksMeta.setDisplayName("Perks");
        profileMeta.setDisplayName(player.getName());
        profileMeta.setOwner(player.getName());
        KDMeta.setDisplayName("K/D");
        AchievmentsMeta.setDisplayName("Achievments");

        clan.setItemMeta(clanMeta);
        perks.setItemMeta(perksMeta);
        profile.setItemMeta(profileMeta);
        KD.setItemMeta(KDMeta);
        Achievments.setItemMeta(AchievmentsMeta);

        inv.setItem(11, clan);
        inv.setItem(15, perks);
        inv.setItem(23, profile);
        inv.setItem(30, KD);
        inv.setItem(34, Achievments);

        player.openInventory(inv);

    }

    @EventHandler
    public void onplayerInventoryclick(InventoryClickEvent event) {
        if (event.getView().getTitle().equals("Statistics")) {
            switch (event.getSlot()) {



                case 11: {//todo openclansGui
                }

                case 15: {
                    //todo openperksGui
                }
                case 23: {
                    //todo openprofileGui
                }

                case 30: {
                    //todo openKDGui
                }

                case 34: {
                    //todo openAchievmentsGui
                }

            }
            event.setCancelled(true);
        }
    }
}
