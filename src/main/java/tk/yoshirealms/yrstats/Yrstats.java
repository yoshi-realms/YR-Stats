package tk.yoshirealms.yrstats;

import org.bukkit.plugin.java.JavaPlugin;
import tk.yoshirealms.yrstats.Gui.GuiStats;

public final class Yrstats extends JavaPlugin {

    public static Yrstats plugin;

    @Override
    public void onEnable() {

        plugin=this;
        getServer().getPluginManager().registerEvents(new GuiStats(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
